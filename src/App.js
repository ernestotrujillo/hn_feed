import React, {useEffect, useState} from 'react';
import './App.css';
import '@aws-amplify/ui/dist/style.css';
import {API} from 'aws-amplify';
import {Segment, Header, Container, Table, Button, Icon, Loader} from 'semantic-ui-react';
import _ from 'lodash';
import Moment from 'react-moment';

const apiName = 'hnFeedApi';
const path = '/articles';
const myInit = { // OPTIONAL
    headers: {}, // OPTIONAL
    response: true, // OPTIONAL (return the entire Axios response object instead of only response.data)
    queryStringParameters: {},
};

function App() {
    const [articles, setArticles] = useState([])
    const [loader, setLoader] = useState(false)

    async function callApi() {

        try {
            setLoader(true);
            const articles = await API.get(apiName, path, myInit)
            setArticles(articles.data)
            setLoader(false);
        } catch (err) {
            setLoader(false);
            console.log({err});
        }
    }

    async function deleteArticle(item){
        try {

            myInit.body = item;
            const response = await API.del(apiName, path, myInit)

            console.log(response);

        } catch (err) {
            console.log({err});
            return err.response;
        }
    }

    useEffect(() => {
        callApi()
    }, [])

    const getColumns = (items) => {
        return _.keys(items[0]);
    }


    const onDelete = (evt, item) => {
        evt.preventDefault();
        evt.stopPropagation();
        // delete from medical conditions
        let purgedArticles = _.reject(articles, item)
        setArticles(purgedArticles)
        deleteArticle(item)
    }

    const calendarStrings = {
        lastDay : '[Yesterday at] LT',
        sameDay : '[Today at] LT',
        nextDay : '[Tomorrow at] LT',
        lastWeek : '[last] dddd [at] LT',
        nextWeek : 'dddd [at] LT',
        sameElse : 'L'
    };

    return (
        <Container className={'App'}>
            <Loader active={loader} />
            <Segment content className="App-header">
                <Header as='h1'>HN Feed</Header>
                <Header as='h2'>We love NodeJs news! </Header>
            </Segment>
            <Table columns={articles.length} celled >
                <Table.Body>
                    {_.map(articles, (item) => (
                        <Table.Row>
                            {
                                getColumns(articles).map((column) => {
                                    if (column !== "created_at"){
                                        return (
                                            <Table.Cell>{item[column]}</Table.Cell>
                                        )
                                    }else{
                                        return (
                                            <Table.Cell>
                                                <Moment calendar={calendarStrings}>
                                                    {item[column]}
                                                </Moment>
                                            </Table.Cell>
                                        )
                                    }
                                })
                            }
                            <Table.Cell>
                                <Button.Group  size='small' vertical>
                                    <Button basic animated='fade' onClick={(e) => onDelete(e, item)} color="red">
                                        <Button.Content visible>Delete</Button.Content>
                                        <Button.Content hidden>
                                            <Icon name='trash alternate outline'/>
                                        </Button.Content>
                                    </Button>

                                </Button.Group>
                            </Table.Cell>

                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        </Container>
    );
}

export default App;

# hn_feed

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## <a name="Requirements"></a>Requirements

- [NodeJs](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/get-npm)
- [Python 3.8](https://www.python.org/downloads/)
- [Amplify](https://docs.amplify.aws/start/getting-started/installation/q/integration/react#option-1-watch-the-video-guide)

## <a name="Instructions"></a>Instructions

- Install NPM (you need to have nodejs previously isntalled)
- Configure Amplify `amplify configure` and follow steps
- Clone the repository 
- Init project 


## Init Project in new account
This will help you start a project from a repo into a new AWS account.

Please make next steps to install packages:

1. `yarn install`
2. `amplify configure` will prompt you to create the new user / credentials.
3. Remove file `/amplify/team-provider-info.json`
3. `amplify init` to initialize project in a new account or existing. Make sure you are using the right AWS credentials 
 in your IDE. 
4. `amplify push` to deploy the project to the new account. 

## Join an existing Project
This will help you join an existing project (Same team AWS account) from a repo.

Please make next steps to install packages:

1. `yarn install`
2. `amplify configure` will prompt you to create the new user / credentials.
3. `amplify init` to initialize project. Make sure you are using the right AWS credentials 
 in your IDE. 
4. `amplify pull` to join the existing project

## Start server

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

## Deployment

### `amplify push` 
Will show the list of pending resources to deploy, say yes and that's it!
![partners view](screenshots/amplify_push.png)


## Publish Application

### `amplify publish` 
This will publish the application in the hosting bucket, but first it will push changes and build the application. 


## Other scripts

### `amplify function add`
With this, you could add a new function to the project and attach it to an API if you want to.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

## Theme Library
This app use the next libraries:
* Theming from Semantic-UI : https://semantic-ui.com/usage/theming.html

* https://github.com/Semantic-Org/Semantic-UI-React
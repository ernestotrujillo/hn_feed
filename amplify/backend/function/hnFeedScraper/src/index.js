/* Amplify Params - DO NOT EDIT
You can access the following resource attributes as environment variables from your Lambda function
var environment = process.env.ENV
var region = process.env.REGION
var storageHnFeedDatabaseName = process.env.STORAGE_HNFEEDDATABASE_NAME
var storageHnFeedDatabaseArn = process.env.STORAGE_HNFEEDDATABASE_ARN

Amplify Params - DO NOT EDIT */
let response;
let environment = process.env.ENV;
let region = process.env.REGION;

exports.handler = async (event) => {

    const axios = require("axios");
    const _ = require("lodash");
    const AWS = require("aws-sdk");

    AWS.config.update({region: region});

    const dynamodb = new AWS.DynamoDB.DocumentClient();

    let tableName = "hnFeedDatabase" + "-" + environment;

    let saveItem = async function (articles) {

        const promises = articles.map(async article => {
            let title = _.isNull(article.title) ? article.story_title : article.title;
            var putItemParams = {
                TableName: tableName,
                Key: {author: article.author, title: title},
                UpdateExpression: 'set created_at = :created_at',
                ExpressionAttributeValues: {
                    ':created_at': article.created_at
                }
            };
            await dynamodb.update(putItemParams).promise().then(function (data) {
                return data;
            })
        })

        return Promise.all(promises)
    };

    let getArticles = async function ()
    {
        let articles = await axios("https://hn.algolia.com/api/v1/search_by_date?query=nodejs");
        return articles;
    }

    try {
        const ret = await getArticles();

        response = await saveItem(ret.data.hits)

        console.log(response);

        return response = {
            'statusCode': 200,
            'body': 'Scraper process succeed'
        }
    } catch (err) {
        console.log(err);
        return err;
    }
};
